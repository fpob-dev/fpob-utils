import pytest

from fpob import utils


@pytest.fixture
def subprocess_run_mock(mocker):
    yield mocker.patch('subprocess.run')


def test_editor(subprocess_run_mock, mocker, tmp_path):
    mocker.patch.dict('os.environ', {'EDITOR': 'vi'})

    filename = tmp_path / 'example'

    utils.editor(filename)

    subprocess_run_mock.assert_called_with(['vi', str(filename)], check=True)


def test_editor_text(subprocess_run_mock, mocker):
    mocker.patch.dict('os.environ', {'EDITOR': 'vi'})

    utils.editor_text('foo')

    subprocess_run_mock.assert_called_with(['vi', mocker.ANY], check=True)


def test_pager(subprocess_run_mock, mocker, tmp_path):
    mocker.patch.dict('os.environ', {'PAGER': 'less'})

    filename = tmp_path / 'example'

    utils.pager(filename)

    subprocess_run_mock.assert_called_with(['less', str(filename)], check=True)


def test_pager_text(subprocess_run_mock, mocker):
    mocker.patch.dict('os.environ', {'PAGER': 'less'})

    utils.pager_text('foo')

    subprocess_run_mock.assert_called_with(['less', mocker.ANY], check=True)
