import pytest

from fpob import utils


@pytest.mark.parametrize(
    'mylist, result',
    [
        ([1, 2, 3], [1, 2, 3]),
        ([1, [2, 3]], [1, 2, 3]),
        ([1, [2, [3]]], [1, 2, 3]),
        # Only list should be flattened
        ([1, (2, 3)], [1, (2, 3)]),
        ([1, {2, 3}], [1, {2, 3}]),
    ],
)
def test_flatten_list(mylist, result):
    assert utils.flatten_list(mylist) == result


@pytest.mark.parametrize(
    'mylist, levels, result',
    [
        ([1, 2, 3], 1, [1, 2, 3]),
        ([1, [2, 3]], 1, [1, 2, 3]),
        ([1, [2, [3]]], 1, [1, 2, [3]]),
    ],
)
def test_flatten_list_levels(mylist, levels, result):
    assert utils.flatten_list(mylist, levels) == result


@pytest.mark.parametrize(
    'mylist, result',
    [
        ([1, 2, 3, 1, 2], [1, 2, 3]),
        ([1, 1, 2, 2, 3], [1, 2, 3]),
        (['1', 1, '2', 2, 3], ['1', 1, '2', 2, 3]),
        ([1.1, 1.2, 1.3], [1.1, 1.2, 1.3]),
    ],
)
def test_deduplicate_list(mylist, result):
    assert utils.deduplicate_list(mylist) == result
